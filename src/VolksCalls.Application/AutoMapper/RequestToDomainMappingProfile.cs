﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using VolksCalls.Domain.Models;
using VolksCalls.Domain.Models.Calls.Request;
using VolksCalls.Domain.Models.CallsCategory.Request;
using VolksCalls.Domain.Models.CallsPreferences;
using VolksCalls.Domain.Models.CI;
using VolksCalls.Domain.Models.CI.Request;

namespace VolksCalls.Application.AutoMapper
{
  public  class RequestToDomainMappingProfile : Profile
    {
        public RequestToDomainMappingProfile()
        {
            CreateMap<CallsOpeningRequest, CallCategoryDomain>();
            CreateMap<CIInsertRequest, CIDomain>();

            CreateMap<CallCategoryInsertRequest, CallsCategoryDomain>()
                       .ForMember(d => d.Description, s => s.MapFrom(m => m.Description))
                       .ForMember(d => d.CIId, s => s.MapFrom(m => m.CiCode))
                       .ForMember(d => d.CallsCategoryParentId, s => s.MapFrom(m => m.CallCategoryParent == null ? null : (Guid?)m.CallCategoryParent.Id))
                ;
        }
    }
}
