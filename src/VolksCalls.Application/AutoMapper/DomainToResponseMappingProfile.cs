﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolksCalls.Domain.Models;
using VolksCalls.Domain.Models.CallsCategory.Response;
using VolksCalls.Domain.Models.CallsPreferences;
using VolksCalls.Domain.Models.CallsPreferences.Response;
using VolksCalls.Domain.Models.CI;
using VolksCalls.Domain.Models.CI.Dto;
using VolksCalls.Domain.Models.CI.Response;
using VolksCalls.Infra.CrossCutting;

namespace VolksCalls.Application.AutoMapper
{
    public class DomainToResponseMappingProfile : Profile
    {

        private string FormatPatchCallsCategory(string Patch)
        => string.Join('/', Patch.Split('|'));
        public DomainToResponseMappingProfile()
        {
            CreateMap<CallCategoryDomain, CallsOpeningPreferencesResponse>();

            CreateMap(typeof(PagedDataResponse<>), typeof(PagedDataResponse<>));

            CreateMap<CIDomain, CIGetResponse>();

            CreateMap<CallsCategoryDomain, CallCategoryInsertResponse>();

            CreateMap<CallsCategoryDomain, CallCategoryUpdateResponse>();

            CreateMap<CIDomain, CIInsertResponse>();
            CreateMap<CIDomain, CIUpdateResponse>();
            

            CreateMap<CIDomain, CIGetDetailsResponse>()
                .ForMember(d => d.CallCategoriesDtos, s => s.MapFrom(m => m.CallsCategories.Select(x=> new CallCategoriesDto { Patch = x.Patch } ) ))
                ;

            //

            CreateMap<CallsCategoryDomain, CallsCategoryGetResponse>()
                .ForMember(d => d.CiCode, s => s.MapFrom(m => m.CI!= null? m.CI.Id.ToString() : "" ))
                .ForMember(d => d.CiName, s => s.MapFrom(m => m.CI != null ? m.CI.CIName : ""))
                .ForMember(d => d.CiId, s => s.MapFrom(m => m.CI != null ? m.CI.CIId : ""))
                .ForMember(d => d.CallGroup, s => s.MapFrom(m => m.CI != null ? m.CI.CallGroup : ""))
                //              .ForMember(d => d.Patch, s => s.MapFrom(m => FormatPatchCallsCategory(m.Patch)))
                ;

        }
    }
}
