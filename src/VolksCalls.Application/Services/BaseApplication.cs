﻿using System;
using System.Collections.Generic;
using System.Text;
using VolksCalls.Application.Interfaces;
using VolksCalls.Domain.Repository;
using VolksCalls.Infra.CrossCutting;

namespace VolksCalls.Application.Services
{
    public class BaseApplication : IBaseApplication
    {

        protected BaseApplication(IUnitOfWork _unitOfWork,
                                  LNotifications _LNotifications)
        {

            if (LNotifications == null)
                LNotifications = new LNotifications();

            unitOfWork = _unitOfWork;

            LNotifications = _LNotifications;
        }

        public IUnitOfWork unitOfWork { get; protected set; }

        public LNotifications LNotifications { get; protected set; }

        public void Dispose() => GC.SuppressFinalize(this);




    }
}
