﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VolksCalls.Domain.Models.Calls
{



   public class CallsDomain: Entity
    {

        public IEnumerable<CallsCategoryDomain> CallsCategories { get; set; }

        public CallsDomain()
        {
            CallsCategories = new List<CallsCategoryDomain>();
        }
    }
}
