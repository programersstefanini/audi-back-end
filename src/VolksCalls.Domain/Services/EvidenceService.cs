﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VolksCalls.Domain.Interfaces;
using VolksCalls.Domain.Models.Evidences.Request;
using VolksCalls.Domain.Models.Evidences.Response;
using VolksCalls.Infra.CrossCutting;
using VolksCalls.Infra.CrossCutting.Emails;

namespace VolksCalls.Domain.Services
{
    public class EvidenceService : BaseService, IEvidenceService
    {

        readonly IConfiguration _configuration;
        readonly IEMailService _iEMailService;
        readonly EmailSendEvidences _mailSettings;
        readonly IUser _user;
        public EvidenceService(IEMailService iEMailService,
                               IOptions<EmailSendEvidences> emailSettings,
                               IUser user, 
                               IConfiguration configuration,
                               LNotifications lNotifications)
            : base(lNotifications)
        {
            _user = user;
                        _configuration = configuration;
            _iEMailService = iEMailService;
            _mailSettings = emailSettings.Value;

        }

        public async Task<SendEvidencesResponse> SendEvidencesAsync(SendEvidencesRequest sendEvidencesRequest, List<IFormFile> files)
        {

            StringBuilder strBody = new StringBuilder();
            var emailRequest = new EmailRequest();
            emailRequest.ToEmails.AddRange(_mailSettings.ToEmails);
            emailRequest.Subject = $@"VW - Envio de Evidência - Requisição {sendEvidencesRequest.RequestNumber} - AUTOATENDIMENTO VW";
            strBody.AppendLine($@"");
            strBody.AppendLine($@"Prezado Service Desk,");
            strBody.AppendLine($@"Seguem anexos arquivos para evidência da requisição {sendEvidencesRequest.RequestNumber}.");
            strBody.AppendLine($@"Arquivos enviados pelo perfil {_user.Name}, através do computador {sendEvidencesRequest.HostName}.");
            strBody.AppendLine($@"Informações adicionais enviadas pelo usuário:{sendEvidencesRequest.InfoAditional}");
            strBody.AppendLine($@"");
            strBody.AppendLine($@"Este e-mail foi enviado via App Service Desk.");
            emailRequest.Body += strBody.ToString();
            emailRequest.Attachments = files;
            await _iEMailService.SendEmailsAsync(emailRequest, _mailSettings);
            return new SendEvidencesResponse();
        }
    }
}
