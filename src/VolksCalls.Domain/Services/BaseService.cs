﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VolksCalls.Domain.Interfaces;
using VolksCalls.Domain.Models;
using VolksCalls.Domain.Repository;
using VolksCalls.Infra.CrossCutting;

namespace VolksCalls.Domain.Services
{

    public abstract class BaseServiceEntity<TEntity> : BaseService,
                          IBaseService<TEntity> where TEntity : EntityDataBase
    {
        public  IBaseRepository<TEntity> _iBaseRepository { get; protected set; }
        

        protected BaseServiceEntity(IBaseRepository<TEntity> iBaseRepository,
                                    LNotifications lNotifications)
                                    :base(lNotifications)

        {
            _iBaseRepository = iBaseRepository;
        }

        protected void SetUpdateEntity<T>(T entity) where T : EntityDataBase
        {

            entity.Active = true;
            entity.DateUpdate = DateTime.Now;
            entity.UserUpdatedId = Guid.NewGuid();


        }

        protected void SetDeleteEntity<T>(T entity) where T : EntityDataBase
        {
            entity.Active = false;
            entity.DeleteDate = DateTime.Now;
            entity.UserDeletedId = Guid.NewGuid();

        }

        protected void SetInsertEntity<T>(T entity) where T : EntityDataBase
        {

            entity.Active = true;
            entity.DateRegister = DateTime.Now;
            entity.UserInsertedId = Guid.NewGuid();

        }

        public void Add(TEntity entidade)
        {
            _iBaseRepository.Add(entidade);
        }

        public void Update(TEntity entidade)
        {
            _iBaseRepository.Update(entidade);
        }

        public void Remove(TEntity entidade)
        {
            _iBaseRepository.Remove(entidade);
        }

        public async Task AddAsync(TEntity entidade)
        {
           await  _iBaseRepository.AddAsync(entidade);
        }

        public async Task AddAsync<T>(T entidade) where T : EntityDataBase
        {
            await _iBaseRepository.AddAsync(entidade);
        }

        public void Update<T>(T entity) where T : EntityDataBase
        {
             _iBaseRepository.Update(entity);
        }

        public void UpdateRange<T>(IEnumerable<T> entity) where T : EntityDataBase
        {
            _iBaseRepository.UpdateRange(entity);
        }
    }



    public abstract class BaseService:IDisposable
    {
        public LNotifications _lNotifications { get; protected set; }

        public BaseService(LNotifications lNotifications)
        {
            _lNotifications = lNotifications;
        }

        public void Dispose() => GC.SuppressFinalize(this);

    }
}
